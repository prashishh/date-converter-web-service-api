﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MvcWebAPI.Models
{
    [Serializable]
    [DataContract]
    public class Date
    {
        [DataMember]
        public string NepaliDate { get; set; }
        [DataMember]
        public string EnglishDate { get; set; }
        [DataMember]
        public string Dated { get; set; }
        [DataMember]
        public string Day { get; set; }
        [DataMember]
        public string Month { get; set; }
        [DataMember]
        public string Year { get; set; }
    }
}
