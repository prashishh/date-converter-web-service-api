﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using MvcWebAPI.Models;
using System.Xml.Linq;
using System.IO;


namespace MvcWebAPI.Controllers.API
{
    public class UserController : Controller
    {
        public ActionResult GetUser(string name)
        {
            return new ObjectResult<User>(new User
            {
                Email = "omaralzabir@gmail.com",
                Name = name
            });
        }

        public ActionResult GetUserList()
        {
            var users = new List<User>();
            users.Add( new User 
                { Email = "ScottGu@microsoft.com", Name="Scott Gurthie" });
            users.Add( new User 
                { Email = "OmarALZabir@gmail.com", Name="Omar AL Zabir" });

            return new ObjectResult<List<User>>(users);
        }

        public ActionResult CreateUser(string name)
        {
            return new ObjectResult<Result>(
                new Result(0, "SUCCESS",
                    new User { Name = name, Email = "scottgu@microsoft.com" })
                , new[] { typeof(User) });
        }

        public ActionResult ReturnError()
        {
            return new ObjectResult<Result>(new Result(1231231, "Some Error Occured"));                
        }

        [ObjectFilter(Param="users",RootType=typeof(User[]))]
        public ActionResult UploadUser(User[] users)
        {
            return new ObjectResult<User[]>(users);
        }
    }
}
