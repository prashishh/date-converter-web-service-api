﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace MvcWebAPI.Rules
{
    public class PathStartsWith : IRouteConstraint
    {
        private string _match = String.Empty;

        public PathStartsWith(string match)
        {
            _match = match;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return httpContext.Request.Url.AbsolutePath.StartsWith(_match, StringComparison.OrdinalIgnoreCase);
        }
    }

    public class PathDoesNotStartWith : IRouteConstraint
    {
        private string _match = String.Empty;

        public PathDoesNotStartWith(string match)
        {
            _match = match;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return !httpContext.Request.Url.AbsolutePath.StartsWith(_match, StringComparison.OrdinalIgnoreCase);
        }
    }
}
