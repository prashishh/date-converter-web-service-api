﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Collections.Specialized;
using MvcWebAPI.Rules;

namespace MvcWebAPI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public const string VIRTUAL_DIRECTORY = "MvcWebAPI/";  // Set it blank for root hosting

        public static void RegisterRoutes(RouteCollection routes)
        {
            var map = new NameValueCollection();
            map.Add("MvcWebAPI.Controllers.API", '/' + VIRTUAL_DIRECTORY + "API");

            ControllerBuilder.Current.SetControllerFactory(
                new ProtectedNamespaceControllerFactory(map));

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "API",                                              // Route name
                "API/{controller}/{action}/{name}",                           // URL with parameters
                new { controller = "Home", action = "Index", name=""},  // Parameter defaults
                new { path = new PathStartsWith('/' + VIRTUAL_DIRECTORY + "API") },
                new string[] { "MvcObjectResult.Controllers.API" });
            
            routes.MapRoute(
                "Default",                                              // Route name
                "{controller}/{action}/{id}",                           // URL with parameters
                new { controller = "Home", action = "Index", id = "" },  // Parameter defaults
                new { path = new PathDoesNotStartWith('/' + VIRTUAL_DIRECTORY + "API") }
            );

        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);
        }
    }
}