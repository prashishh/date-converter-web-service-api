﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using MvcWebAPI.Models;
using System.Xml.Linq;
using System.IO;


namespace MvcWebAPI.Controllers.API
{
    public class DateController : Controller
    {
        public ActionResult GetEnglish(String date)
        {
            if (!String.IsNullOrEmpty(date) && date.Length.Equals (8))
            {
                NepToEngDate dt = new NepToEngDate();
                int f_year = 0;
                int f_day = 0;
                int f_month = 0;

                f_year = Convert.ToInt16(date.Substring(0, 4));
                f_month = Convert.ToInt16(date.Substring(4, 2));
                f_day = Convert.ToInt16(date.Substring(6, 2));
                String result = dt.DateConversion(f_day, f_month, f_year);
                String c_year = dt.getyear().ToString();
                String c_month = dt.getmonth().ToString();
                String c_date = dt.getdate();
                String c_day = dt.dayOfWeek.ToString();

                return new ObjectResult<Date>(new Date
                {
                    NepaliDate = date,
                    EnglishDate = result,
                    Dated = c_date,
                    Month = c_month,
                    Year = c_year,
                    Day = c_day
                });
            }
            else
            {
                return new ObjectResult<Date>(new Date
                {
                    NepaliDate = date,
                    EnglishDate = "Invalid Date"
                });
            }
        }

        public ActionResult GetNepali(String date)
        {
            if (!String.IsNullOrEmpty(date) && date.Length.Equals(8))
            {
                EngToNepDate dt = new EngToNepDate();
                int f_year = 0;
                int f_day = 0;
                int f_month = 0;

                f_year = Convert.ToInt16(date.Substring(0, 4));
                f_month = Convert.ToInt16(date.Substring(4, 2));
                f_day = Convert.ToInt16(date.Substring(6, 2));
               
                String result = dt.Nepdate(f_day, f_month, f_year);
                String c_year = dt.getyear().ToString();
                String c_month = dt.getmonth().ToString();
                String c_date = dt.getdate().ToString();
                String c_day = dt.getDay().ToString();

                return new ObjectResult<Date>(new Date
                {
                    NepaliDate = date,
                    EnglishDate = result,
                    Dated = c_date,
                    Month = c_month,
                    Year = c_year,
                    Day = c_day
                });
            }
            else
            {
                return new ObjectResult<Date>(new Date
                {
                    NepaliDate = date,
                    EnglishDate = "Invalid Date"
                });
            }
        }
    }
}
