﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="MvcWebAPI.Views.Home.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" src="/MvcWebAPI/Content/MicrosoftAjax.js" ></script>
<script type="text/javascript" >
    function buildQueryString(params) {
        if (null == params) return "";
        query = "";
        for (var key in params)
            query += key + "=" + escape(params[key]);
        return query;
    }

    function getUrl(url, params, contentType, output, body) {
        var fullUrl = url + "?" + buildQueryString(params);
        var request = new Sys.Net.WebRequest();
        request.get_headers()['Content-Type'] = contentType;
        request.set_url(url);
        if (typeof body != "undefined") {
            request.set_httpVerb("POST");
            request.set_body(body);
        }
        request.add_completed(Function.createDelegate(null, function(executor) {
            if (executor.get_responseAvailable()) {
                var data = executor.get_responseData();
                $get(output).innerText = data;
                $get(output).textContent = data;
            }
        }));
        request.invoke();
    }
    
    function makeAJAXCall(url, params, output) {
        getUrl(url, params, 'application/json; charset=utf-8', output);
    }

    function makeRESTCall(url, params, output) {
        getUrl(url, params, 'text/xml', output);
    }

    function postXml(url, params, xml, output) {
        getUrl(url, params, 'text/xml', output, xml);
    }
    function postJson(url, params, xml, output) {
        getUrl(url, params, 'application/json; charset=utf-8', output, xml);
    }
</script>

<h2>GetUser</h2>
<p>/API/User/GetUser</p>
<p>Returns a single User object</p>
<input type="button" value="AJAX" onclick="makeAJAXCall('/MvcWebAPI/API/User/GetUser/Omar', null, 'GetUser_Result')" />
<input type="button" value="XML" onclick="makeRESTCall('/MvcWebAPI/API/User/GetUser/Scott+Guthrie', null, 'GetUser_Result')" />
<pre id="GetUser_Result"></pre>
<hr />

<h2>GetUserList</h2>
<p>Returns a user collection</p>
<input type="button" value="AJAX" onclick="makeAJAXCall('/MvcWebAPI/API/User/GetUserList', null, 'GetUserList_Result')" />
<input type="button" value="XML" onclick="makeRESTCall('/MvcWebAPI/API/User/GetUserList', null, 'GetUserList_Result')" />
<pre id="GetUserList_Result"></pre>
<hr />

<h2>CreateUser</h2>
<p>Creates a user and returns a generic result object</p>
<input type="button" value="AJAX" onclick="makeAJAXCall('/MvcWebAPI/API/User/CreateUser/Omar', null, 'CreateUser_Result')" />
<input type="button" value="XML" onclick="makeRESTCall('/MvcWebAPI/API/User/CreateUser/Scott+Guthrie', null, 'CreateUser_Result')" />
<pre id="CreateUser_Result"></pre>
<hr />

<h2>UploadUser</h2>
<p>Uploads the following XML or JSON using HTTP POST</p>
<textarea id="Xml" rows="10" cols="30">
&lt;ArrayOfUser&gt;
    &lt;User&gt;
        &lt;Name&gt;Scott Guthrie&lt;/Name&gt;
        &lt;Email&gt;scottgu@microsoft.com&lt;/Email&gt;
    &lt;/User&gt;
    &lt;User&gt;
        &lt;Name&gt;Omar AL Zabir&lt;/Name&gt;
        &lt;Email&gt;omaralzabir@gmail.com&lt;/Email&gt;
    &lt;/User&gt;
&lt;/ArrayOfUser&gt;    
</textarea>
<input type="button" value="Post Xml" onclick="postXml('/MvcWebAPI/API/User/UploadUser', null, $get('Xml').value, 'UploadUser_Result' )" />
<textarea id="Json" rows="10" cols="30">
[{"Email":"omaralzabir@gmail.com","Name":"Omar AL Zabir"},
{"Email":"scottgu@microsoft.com","Name":"Scott Guthrie"}]
</textarea>
<input type="button" value="Post Json" onclick="postJson('/MvcWebAPI/API/User/UploadUser', null, $get('Json').value, 'UploadUser_Result' )" />

<pre id="UploadUser_Result"></pre>


</asp:Content>
